import React, { useState, useEffect } from 'react'
import '../app.scss'
import './contributors.scss'
import API from '../../backend/API'
import ReactDOM from 'react-dom'
const fileSystem = require("browserify-fs")
const Contributors = () => {

 let contribs = "avant"
let dev = []

    //State
let contribes = []
    //Get all clients on load
fetch("./contributors.json")
  .then(response => response.json())
  .then(data => {
    for(let i=0;i<data.Contributors.length;i++){
        contribes[i] = data.Contributors[i].Name;
    }
    let lstCtrb = document.getElementById('contributor__list')
    for(let i=0;i<contribes.length;i++){
       let liCtrb = document.createElement('li')
    liCtrb.innerHTML = contribes[i]
    lstCtrb.appendChild( liCtrb ) 
    }

    
    
})
  .catch(error => console.log("erreur",error));

fetch("./developpers.json")
  .then(response => response.json())
  .then(data => {
    for(let i=0;i<data.Developpers.length;i++){
        dev[i] = data.Developpers[i].Name;
    }
    let lstDev = document.getElementById('developper__list')
    for(let i=0;i<dev.length;i++){
       let liDev = document.createElement('li')
    liDev.innerHTML = dev[i]
    lstDev.appendChild( liDev ) 
    }

    
    
})
  .catch(error => console.log("erreur",error));



console.log("result",contribs);

    return (
        <section className="flop__contributors">
            <div className="container">
                <div className="contributors__header">


                    <h1>Les développeurs du projet</h1>
<p>Retrouvez ci-dessous les développeurs principeaux de l'application.</p>
<ul id="developper__list" className="developpers" >
                        
                        </ul>
                    <h1>Les contributeurs du projet</h1>
                    <p>Retrouvez ci-dessous la liste de toutes les personnes ayant participé de près ou de loin au développement de l'application.</p>
                </div>
                <div className="contributors__category">

                    <div className="row">
                        <div className="contributor__card">                 

                        
                        <ul id="contributor__list" className="contributors" >
                        
                        </ul>
                              
                        </div>
                    </div>
                </div>
                <div className="contributors__tutorial">
                    <h1>Comment contribuer ?</h1>
                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Adipisci corporis culpa cupiditate, dolorem dolores enim eos error harum, illum ipsa nam odit perspiciatis placeat repellat repellendus, soluta vel vero. Voluptate.</p>
                </div>
            </div>
        </section>
    )
}
   

export default Contributors
